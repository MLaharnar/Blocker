﻿using UnityEngine;

[CreateAssetMenu]
public class ScienceArgsVar:ScriptableObject {
    public ScienceArgs args;
}
