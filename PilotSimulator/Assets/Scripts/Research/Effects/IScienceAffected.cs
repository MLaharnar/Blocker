﻿using System.Collections.Generic;
using UnityEngine;

public abstract class ScienceAffected :MonoBehaviour{
    public ScienceArgsVar ScienceTree;
    public List<ScienceEffect> Effects;
}