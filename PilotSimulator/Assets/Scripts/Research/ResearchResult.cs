﻿using UnityEngine;

[System.Serializable]
public class ResearchResult {
    public Vector3 recordedRelativePosition;
    public Vector3 lastImportantPoint;
    public string type;
    public float value;

}
