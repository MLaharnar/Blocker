﻿using UnityEngine;

[CreateAssetMenu]
public class TransformRef : ScriptableObject {
    public Transform value;
}
