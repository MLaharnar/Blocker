﻿using UnityEngine;

[CreateAssetMenu()]
public class BoolVar : ScriptableObject {
    public bool value;

}
