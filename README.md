# Blocker
**Game about builder blocks and researchable AI.**

Everything should be researchable. Movement, rotation, health points, flying, targeting, spawning... Research should be editable. Movement is 90% effective. Spawning 30% effective. Building are created with basic blocks and research.

It should be like playing with legos.

This allows for mixing results. For example mixing movement, with rotation and targeting give us fliers. Spawning plus rotation plus gravity should give us barriers.

---

First stage: 

--> ??

Basic prototype that shows the idea, without actual gameplay goals.

Features:
- [x] research menu with items that can be edited
- [x] units that can move, target, rotate depending on picked research.
- [x] information map (like x-ray vision)
- [x] units can spawn, die and give money.

Results:
![Stage 1](/images/stage1.png)
Format: ![Alt Text](url)

---

Second stage:

Gameplay goals, experimentation, understanding of key time sinks in this sort of project.

--> 28.03.2020

 In progress.

Features:
- [ ] offers one full simple level to beat.
- [ ] ui, background.
- [ ] explore a few ideas around using different blocks.

---

Third stage:

More experimentation based on speed of development. Developing a few features to feature a few levels.

Upcoming.
